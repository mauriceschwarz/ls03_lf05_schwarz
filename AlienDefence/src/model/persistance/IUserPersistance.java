package model.persistance;

import model.User;

public interface IUserPersistance {

	User readUser(String username);
	int createUser(User user);
	void updateUser(User user);
	void deleteUser(int P_user_id);

}