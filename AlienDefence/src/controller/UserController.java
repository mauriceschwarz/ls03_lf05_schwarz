package controller;

import model.User;
import model.persistance.IUserPersistance;

/**
 * controller for users
 * @author Clara Zufall
 * TODO JavaDoc vervollständigen
 */
public class UserController {

	private IUserPersistance userPersistance;

	
	public UserController(IUserPersistance userPersistance) {
		this.userPersistance = userPersistance;
	}

	/**
	 *
	 * @param user
	 * @return
	 */
	public int createUser(User user) { return this.userPersistance.createUser(user); }
	
	/**
	 * liest einen User aus der Persistenzschicht und gibt das Userobjekt zur�ck
	 * @param username eindeutige Loginname
	 * @return Userobjekt, null wenn der User nicht existiert
	 */
	public User readUser(String username) {
		return this.userPersistance.readUser(username);
	}

	/**
	 * Updatet einen Benutzer
	 * @param user
	 */
	public void changeUser(User user) { this.userPersistance.updateUser(user); }

	/**
	 * Entfernt einen User anhand der eingelesen ID
	 * @param user
	 */
	public void deleteUser(User user) { this.userPersistance.deleteUser(user.getP_user_id()); }
	/**
	 * Vergleicht Übergebende Daten mit User aus DB
	 * @param username
	 * @param passwort
	 * @return true wenn das Passwort korrekt ist, false wenn nicht
	 */
	public boolean checkPassword(String username, String passwort) {
		return this.userPersistance.readUser(username).getPassword().equals(passwort);
	}
}
