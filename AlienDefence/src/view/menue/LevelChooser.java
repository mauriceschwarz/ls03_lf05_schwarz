package view.menue;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import controller.AlienDefenceController;
import controller.GameController;
import controller.LevelController;
import model.Level;
import model.User;
import view.game.GameGUI;

@SuppressWarnings("serial")
public class LevelChooser extends JPanel {

	private LevelController lvlControl;
	private LeveldesignWindow leveldesignWindow;
	private JTable tblLevels;
	private DefaultTableModel jTableData;

	/**
	 * Create the panel.
	 * 
	 * @param leveldesignWindow
	 */
	public LevelChooser(AlienDefenceController alienDefenceController, LeveldesignWindow leveldesignWindow, String origin, User user) {
		this.lvlControl = alienDefenceController.getLevelController();
		this.leveldesignWindow = leveldesignWindow;

		setLayout(new BorderLayout());

		JPanel pnlButtons = new JPanel();
		add(pnlButtons, BorderLayout.SOUTH);

		JButton btnPlay = new JButton("Spielen");
		btnPlay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnPlay_Clicked(alienDefenceController, user);
			}
		});
		pnlButtons.add(btnPlay);

		JButton btnHighscore = new JButton("Highscore");
		btnHighscore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//new Highscore(alienDefenceController.getAttemptController(), cboLevelChooser.getSelectedIndex());
			btnHighscore_Clicked(alienDefenceController, user);
			}
		});
		pnlButtons.add(btnHighscore);
		
		JButton btnNewLevel = new JButton("Neues Level");
		btnNewLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnNewLevel_Clicked();
			}
		});
		pnlButtons.add(btnNewLevel);

		JButton btnUpdateLevel = new JButton("ausgew\u00E4hltes Level bearbeiten");
		btnUpdateLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnUpdateLevel_Clicked();
			}
		});
		pnlButtons.add(btnUpdateLevel);

		JButton btnDeleteLevel = new JButton("Level l\u00F6schen");
		btnDeleteLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnDeleteLevel_Clicked();
			}
		});
		pnlButtons.add(btnDeleteLevel);

		JLabel lblLevelauswahl = new JLabel("Levelauswahl");
		lblLevelauswahl.setFont(new Font("Arial", Font.BOLD, 18));
		lblLevelauswahl.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblLevelauswahl, BorderLayout.NORTH);

		JScrollPane spnLevels = new JScrollPane();
		add(spnLevels, BorderLayout.CENTER);

		tblLevels = new JTable();
		tblLevels.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		spnLevels.setViewportView(tblLevels);
		//Ans Hauptmenü angleichen
		spnLevels.getViewport().setBackground(Color.BLACK);
		tblLevels.setForeground(Color.ORANGE);
		pnlButtons.setBackground(Color.BLACK);
		lblLevelauswahl.setForeground(new Color(124, 252, 0));
		spnLevels.getViewport().setBackground(Color.BLACK);
		tblLevels.setBackground(Color.BLACK);

		this.updateTableData();

		if (origin.equals("Spielen")){
			btnNewLevel.setVisible(false);
			btnUpdateLevel.setVisible(false);
			btnDeleteLevel.setVisible(false);
		}
		else if (origin.equals("Leveleditor"))
		{
			btnPlay.setVisible(false);
			btnHighscore.setVisible(false);
		}
	}

	private String[][] getLevelsAsTableModel() {
		List<Level> levels = this.lvlControl.readAllLevels();
		String[][] result = new String[levels.size()][];
		int i = 0;
		for (Level l : levels) {
			result[i++] = l.getData();
		}
		return result;
	}

	public void updateTableData() {
		this.jTableData = new DefaultTableModel(this.getLevelsAsTableModel(), Level.getLevelDescriptions());
		this.tblLevels.setModel(jTableData);
	}

	public void btnNewLevel_Clicked() {
		this.leveldesignWindow.startLevelEditor();
	}

	public void btnUpdateLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.leveldesignWindow.startLevelEditor(level_id);
	}

	public void btnDeleteLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.lvlControl.deleteLevel(level_id);
		this.updateTableData();
	}
	public void btnPlay_Clicked(AlienDefenceController alienDefenceController, User user) {
		//User user = new User(1, "test", "pass");

		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		Level level = alienDefenceController.getLevelController().readLevel(level_id);

		Thread t = new Thread("GameThread") {
			@Override
			public void run() {
				GameController gameController = alienDefenceController.startGame(level, user);
				new GameGUI(gameController).start();
			}
		};
		t.start();
	}

	public void btnHighscore_Clicked(AlienDefenceController alienDefenceController, User user)
	{
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));

		new Highscore(alienDefenceController.getAttemptController(), level_id);
	}
}
