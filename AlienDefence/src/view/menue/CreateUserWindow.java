package view.menue;

import controller.AlienDefenceController;
import controller.UserController;
import model.User;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.time.LocalDate;

//TODO create a usermanagement
public class CreateUserWindow extends JFrame  {

	private UserController userController;
	private JTextField txtPostleitzahl;
	private JTextField txtFamilienstand;
	private JTextField txtGehaltserwartungen;
	private JTextField txtStadt;
	private JTextField txtHausnummer;
	private JTextField txtStraße;
	private JTextField txtGeburtstag;
	private JTextField txtNachname;
	private JTextField txtVorname;
	private JTextField txtAbschlussnote;
	private JPasswordField passwordField;
	private JTextField txtUsername;

	public CreateUserWindow(AlienDefenceController alienDefenceController) {
		this.userController=alienDefenceController.getUserController();

		setTitle("Benutzerverwaltung");
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setBounds(100, 100, 515, 554);
		setContentPane(contentPane);
		JLabel lblUserLabel = new JLabel("Lege einen neuen User an");
		lblUserLabel.setBounds(179, 6, 163, 16);
		contentPane.add(lblUserLabel);

		//Buttons
		JButton btnNewButton = new JButton("Speichern");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				createUserClicked();
			}
		});
		contentPane.setLayout(null);
		btnNewButton.setBounds(162, 449, 154, 47);
		getContentPane().add(btnNewButton);

		//Todo Form validation einbauen
		//Form
		txtNachname = new JTextField();
		txtNachname.setText("");
		txtNachname.setBounds(179, 100, 137, 41);
		contentPane.add(txtNachname);
		txtNachname.setColumns(10);
		
		txtGeburtstag = new JTextField();
		txtGeburtstag.setBounds(332, 191, 137, 41);
		contentPane.add(txtGeburtstag);
		txtGeburtstag.setColumns(10);

		txtStraße = new JTextField();
		txtStraße.setText("");
		txtStraße.setBounds(16, 191, 137, 41);
		contentPane.add(txtStraße);
		txtStraße.setColumns(10);
		
		txtStadt = new JTextField();
		txtStadt.setText("");
		txtStadt.setBounds(16, 272, 137, 41);
		contentPane.add(txtStadt);
		txtStadt.setColumns(10);
		
		txtGehaltserwartungen = new JTextField();
		txtGehaltserwartungen.setText("");
		txtGehaltserwartungen.setBounds(332, 272, 137, 41);
		contentPane.add(txtGehaltserwartungen);
		txtGehaltserwartungen.setColumns(10);
		
		txtFamilienstand = new JTextField();
		txtFamilienstand.setText("");
		txtFamilienstand.setBounds(332, 100, 137, 41);
		contentPane.add(txtFamilienstand);
		txtFamilienstand.setColumns(10);
		
		txtPostleitzahl = new JTextField();
		txtPostleitzahl.setText(" ");
		txtPostleitzahl.setBounds(179, 272, 137, 41);
		contentPane.add(txtPostleitzahl);
		txtPostleitzahl.setColumns(10);
		
		txtHausnummer = new JTextField();
		txtHausnummer.setText("");
		txtHausnummer.setBounds(179, 191, 137, 41);
		contentPane.add(txtHausnummer);
		txtHausnummer.setColumns(10);
		
		txtVorname = new JTextField();
		txtVorname.setText("");
		txtVorname.setBounds(16, 100, 137, 41);
		contentPane.add(txtVorname);
		txtVorname.setColumns(10);
		
		txtAbschlussnote = new JTextField();
		txtAbschlussnote.setText("");
		txtAbschlussnote.setBounds(332, 360, 137, 41);
		contentPane.add(txtAbschlussnote);
		txtAbschlussnote.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(179, 360, 137, 41);
		contentPane.add(passwordField);
		
		txtUsername = new JTextField();
		txtUsername.setText("");
		txtUsername.setBounds(16, 360, 137, 41);
		contentPane.add(txtUsername);
		txtUsername.setColumns(10);
		//labels
		
		JLabel lblNewLabel = new JLabel("Stadt");
		lblNewLabel.setBounds(51, 244, 61, 16);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Postleitzahl");
		lblNewLabel_1.setBounds(189, 244, 109, 16);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Gehaltserwartungen");
		lblNewLabel_2.setBounds(342, 244, 127, 16);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Straße");
		lblNewLabel_3.setBounds(51, 163, 61, 16);
		contentPane.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Hausnummer");
		lblNewLabel_4.setBounds(189, 163, 109, 16);
		contentPane.add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Geburtstag");
		lblNewLabel_5.setBounds(342, 163, 87, 16);
		contentPane.add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("Vorname");
		lblNewLabel_6.setBounds(51, 72, 61, 16);
		contentPane.add(lblNewLabel_6);
		
		JLabel lblNewLabel_7 = new JLabel("Nachname");
		lblNewLabel_7.setBounds(214, 72, 84, 16);
		contentPane.add(lblNewLabel_7);
		
		JLabel lblNewLabel_8 = new JLabel("Loginame");
		lblNewLabel_8.setBounds(51, 336, 61, 16);
		contentPane.add(lblNewLabel_8);
		
		JLabel lblNewLabel_9 = new JLabel("Passwort");
		lblNewLabel_9.setBounds(214, 332, 61, 16);
		contentPane.add(lblNewLabel_9);
		
		JLabel lblNewLabel_10 = new JLabel("Abschlussnote");
		lblNewLabel_10.setBounds(368, 332, 61, 16);
		contentPane.add(lblNewLabel_10);
		
		JLabel lblNewLabel_11 = new JLabel("Familienstand");
		lblNewLabel_11.setBounds(345, 72, 107, 16);
		contentPane.add(lblNewLabel_11);

		this.setVisible(true);
	}
	public void createUserClicked() {
		User newUser = userController.readUser("test");
		newUser.setFirst_name(txtVorname.getText());
		newUser.setSur_name(txtNachname.getText());
		newUser.setStreet(txtStraße.getText());
		newUser.setFinal_grade(Integer.parseInt(txtAbschlussnote.getText()));
		newUser.setHouse_number(txtHausnummer.getText());
		newUser.setMarital_status(txtFamilienstand.getText());
		newUser.setPostal_code(txtPostleitzahl.getText());
		newUser.setBirthday(LocalDate.parse(txtGeburtstag.getText()));
		newUser.setSalary_expectations(Integer.parseInt(txtGehaltserwartungen.getText()));
		newUser.setCity(txtStadt.getText());
		newUser.setLoginname(txtUsername.getText());
		newUser.setPassword(new String(passwordField.getPassword()));

		userController.createUser(newUser);
		dispose();
		JOptionPane.showMessageDialog(null, "Benutzer Erfolgreich angelegt", "", JOptionPane.INFORMATION_MESSAGE);
	}
}
