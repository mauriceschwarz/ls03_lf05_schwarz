package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JColorChooser;

import java.awt.Button;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;

public class Gui extends JFrame {

	private JPanel contentPane;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gui frame = new Gui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Gui() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(150, 150, 450, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Dieser Text soll verändert werden.");
		lblNewLabel.setBounds(123, 16, 263, 16);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Aufgabe 1: Hintergrundfarbe ändern");
		lblNewLabel_1.setBounds(28, 56, 395, 16);
		contentPane.add(lblNewLabel_1);
		
		JButton btnNewButton = new JButton("Rot");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.RED);
			}
		});
		btnNewButton.setBounds(6, 84, 117, 29);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Grün");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.GREEN);
			}
		});
		btnNewButton_1.setBounds(135, 84, 117, 29);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Blau");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.BLUE);
			}
		});
		btnNewButton_2.setBounds(264, 84, 117, 29);
		contentPane.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("Gelb");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.YELLOW);
			}
		});
		btnNewButton_3.setBounds(6, 125, 117, 29);
		contentPane.add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("Standart");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(UIManager.getColor("Panel.backround"));
			}
		});
		btnNewButton_4.setBounds(135, 125, 117, 29);
		contentPane.add(btnNewButton_4);
		
		JButton btnNewButton_5 = new JButton("Farbe wählen");
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JColorChooser.showDialog(contentPane, "Wähle Farbe", Color.WHITE);
			}
		});
		btnNewButton_5.setBounds(264, 125, 117, 29);
		contentPane.add(btnNewButton_5);
		
		JLabel lblNewLabel_2 = new JLabel("Text formatieren");
		lblNewLabel_2.setBounds(16, 168, 159, 16);
		contentPane.add(lblNewLabel_2);
		
		JButton btnNewButton_6 = new JButton("Arial");
		btnNewButton_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setFont(new java.awt.Font("Arial", 0, 14));;
			}
		});
		btnNewButton_6.setBounds(6, 196, 117, 29);
		contentPane.add(btnNewButton_6);
		
		JButton btnNewButton_7 = new JButton("Comic Sans...");
		btnNewButton_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setFont(new java.awt.Font("Comic Sans", 0, 14));;
			}
		});
		btnNewButton_7.setBounds(135, 196, 117, 29);
		contentPane.add(btnNewButton_7);
		
		JButton btnNewButton_8 = new JButton("Courier New");
		btnNewButton_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setFont(new java.awt.Font("Courier New", 0, 14));;
			}
		});
		btnNewButton_8.setBounds(264, 196, 117, 29);
		contentPane.add(btnNewButton_8);
		
		textField = new JTextField();
		textField.setBounds(16, 226, 361, 26);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton_9 = new JButton("Ins Label Schreiben");
		btnNewButton_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setText(textField.getText());
			}
		});
		btnNewButton_9.setBounds(44, 264, 144, 29);
		contentPane.add(btnNewButton_9);
		
		JButton btnNewButton_10 = new JButton("Text im Label löschen");
		btnNewButton_10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setText("");
			}
		});
		btnNewButton_10.setBounds(200, 264, 144, 29);
		contentPane.add(btnNewButton_10);
		
		JLabel lblNewLabel_3 = new JLabel("Aufagbe 3: Schriftfarbe ändern");
		lblNewLabel_3.setBounds(16, 305, 236, 26);
		contentPane.add(lblNewLabel_3);
		
		JButton btnNewButton_11 = new JButton("Rot");
		btnNewButton_11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setForeground(Color.RED);
			}
		});
		btnNewButton_11.setBounds(6, 343, 117, 29);
		contentPane.add(btnNewButton_11);
		
		JButton btnNewButton_12 = new JButton("Blau");
		btnNewButton_12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setForeground(Color.BLUE);
			}
		});
		btnNewButton_12.setBounds(135, 343, 117, 29);
		contentPane.add(btnNewButton_12);
		
		JButton btnNewButton_13 = new JButton("Schwarz");
		btnNewButton_13.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setForeground(Color.BLACK);
			}
		});
		btnNewButton_13.setBounds(269, 343, 117, 29);
		contentPane.add(btnNewButton_13);
		
		JLabel lblNewLabel_4 = new JLabel("Aufagbe 4: Schriftgröße verändern");
		lblNewLabel_4.setBounds(16, 399, 263, 16);
		contentPane.add(lblNewLabel_4);
		
		JButton btnNewButton_14 = new JButton("+");
		btnNewButton_14.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setFont
				(new java.awt.Font(lblNewLabel.getFont().getFontName(), 0, (lblNewLabel.getFont().getSize() + 1)));
			}
		});
		btnNewButton_14.setBounds(6, 425, 192, 29);
		contentPane.add(btnNewButton_14);
		
		JButton btnNewButton_15 = new JButton("-");
		btnNewButton_15.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setFont
				(new java.awt.Font(lblNewLabel.getFont().getFontName(), 0, (lblNewLabel.getFont().getSize() - 1)));
			}
		});
		btnNewButton_15.setBounds(210, 425, 176, 29);
		contentPane.add(btnNewButton_15);
		
		JLabel lblNewLabel_5 = new JLabel("Aufgabe 5: Textausrichtung");
		lblNewLabel_5.setBounds(16, 466, 215, 16);
		contentPane.add(lblNewLabel_5);
		
		JButton btnNewButton_16 = new JButton("linksbündig");
		btnNewButton_16.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setHorizontalAlignment(JLabel.LEFT);
			}
		});
		btnNewButton_16.setBounds(6, 503, 117, 29);
		contentPane.add(btnNewButton_16);
		
		JButton btnNewButton_17 = new JButton("zentriert");
		btnNewButton_17.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setHorizontalAlignment(JLabel.CENTER);
			}
		});
		btnNewButton_17.setBounds(135, 503, 117, 29);
		contentPane.add(btnNewButton_17);
		
		JButton btnNewButton_18 = new JButton("rechtsbündig");
		btnNewButton_18.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setHorizontalAlignment(JLabel.RIGHT);
			}
		});
		btnNewButton_18.setBounds(264, 503, 117, 29);
		contentPane.add(btnNewButton_18);
		
		JLabel lblNewLabel_6 = new JLabel("Aufgabe 6: Programme beenden");
		lblNewLabel_6.setBounds(6, 554, 273, 16);
		contentPane.add(lblNewLabel_6);
		
		JButton btnNewButton_19 = new JButton("EXIT");
		btnNewButton_19.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnNewButton_19.setBounds(6, 582, 402, 84);
		contentPane.add(btnNewButton_19);
		
		
	}
}
